import pytest
from arcanum.replacers import replace_numbers

array_with_constant_ints = [1, 1, 1, 1]
array_with_ordered_ints = [1, 2, 3, 4]
array_with_random_ints = ["3", "", "2", "1", "", "4"]

array_with_constant_floats = [1.234, 1.234, 1.234, 1.234]
array_with_ordered_floats = ["", "1.1", "2.2", "3.3", ""]
array_with_random_floats = [4.4, 1.1, 2.2, 3.3]


def test_replace_numbers_returns_correct_length():
    assert len(array_with_ordered_ints) == 4
    assert len(array_with_random_ints) == 6
    assert len(array_with_ordered_floats) == 5
    assert len(array_with_random_floats) == 4


def test_replace_numbers_returns_constants():
    assert len(set(replace_numbers(array_with_constant_ints))) == 1
    assert len(set(replace_numbers(array_with_constant_floats))) == 1


def test_replace_numbers_raises_error():
    with pytest.raises(ValueError):
        replace_numbers("This is a string")
