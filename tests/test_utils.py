from arcanum.utils import format_number, which_not_none


def test_format_number_is_none():
    assert format_number("") == None


def test_format_number_is_int():
    assert format_number("10") == 10
    assert format_number("-10") == 10


def test_format_number_is_float():
    assert format_number("0.1") == 0.1
    assert format_number("-0.1") == 0.1


def test_format_number_infinite_is_none():
    assert format_number("-Inf") == None
    assert format_number("Inf") == None


def test_which_not_none():
    assert which_not_none([1, 2, 3, None, 5, 6, None]) == [0, 1, 2, 4, 5]
