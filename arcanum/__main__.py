import os
import csv
import shutil
import traceback
from datetime import datetime
from .utils import format_number
from .replacers import replace_numbers

this_dir = os.getcwd()
now = datetime.now().strftime("%Y_%m_%d_%H%M%S")


def main():
    zip_name = f"{this_dir}_{now}"
    shutil.make_archive(zip_name, "zip", this_dir)
    print(f"Backup created in parent directory: {zip_name}.zip")

    file_names = []
    counter = 0
    errors = 0
    passes = 0

    for root, dirs, files in os.walk(this_dir):
        for file in files:
            if file.endswith(".csv"):
                counter += 1
                file_names.append(file)
                data = []

                try:
                    with open(os.path.join(root, file), "rU") as in_file:
                        print(f"Obfuscating file: {os.path.join(root, file)}")
                        reader = csv.reader(in_file)
                        all_rows = [row for row in reader]
                        num_cols = len(all_rows[0])
                        for i in range(0, num_cols):
                            column = [col[i] for col in all_rows]
                            column_name = column.pop(0)
                            column = [format_number(row) for row in column]
                            column_replaced = replace_numbers(column)
                            column_replaced.insert(0, column_name)
                            data.append(column_replaced)

                    data = list(zip(*data))

                    with open(os.path.join(root, file), "w", newline="") as in_file:
                        writer = csv.writer(in_file)
                        writer.writerows(data)

                    passes += 1

                except Exception as e:
                    print(f"Failed to obfuscate file: {os.path.join(root, file)}")
                    traceback.print_exc()
                    errors += 1

    print(
        f"{passes} file(s) obfuscated (error: {errors}, total: {counter}) in {this_dir}"
    )


if __name__ == "__main__":
    main()
