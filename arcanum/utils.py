import numbers
import math
from random import randint


def random_generator(min=0, max=1):
    """Generates a single random int between min and max."""
    while True:
        yield randint(min, max)


def replacement_generator(replacements):
    """Constructs a generator using an array of ints or floats."""
    for rep in replacements:
        yield rep


def format_number(n):
    """Formats a single element into an int or a float (rounded to 4 decimals)."""
    if isinstance(n, numbers.Number):
        return n
    elif len(n) > 0:
        try:
            return abs(int(n))
        except ValueError:
            try:
                value = round(float(n), 4)
                return abs(value) if math.isfinite(value) else None
            except ValueError:
                return n
    else:
        return None


which_not_none = lambda arr: [
    i for i, val in enumerate(arr) if isinstance(val, (int, float))
]
