from random import randint, uniform
from .utils import random_generator, replacement_generator, which_not_none


def replace_ints(arr, scalar=2):
    """Replaces an array of ints with random ints.

    Replaces and array of ints (that can include None's) with random ints.
    Nones are kept in place and if the array is ordered, the returned array
    will be ordered as well.

    Args:
        arr: An array to be replaced.
        scalar: An int which represents how much to scale the replaced values by.
            This is dependent on the min and max values of the ints in the input
            array `arr`. 
    
    Returns:
        An array with the same lenght as the input array `arr` with randomised
        ints.
    """
    indexes = which_not_none(arr)
    keep = [arr[i] for i in indexes]
    length = len(keep)
    minima, maxima = (min(keep), max(keep))
    if minima == maxima:
        CONST_NUMBERS = [
            randint(round(minima / scalar), round(maxima * scalar))
        ] * length
        results = replacement_generator(CONST_NUMBERS)
        return [next(results) if n is not None else None for n in arr]
    else:
        if keep == sorted(keep):
            try:
                full_set = sample(
                    range(round(minima / scalar), round(maxima * scalar)), length
                )
            except:
                return_random = random_generator(
                    round(minima / scalar), round(maxima * scalar)
                )
                full_set = [next(return_random) for i in keep]
            full_set.sort()
            results = replacement_generator(full_set)
            return [next(results) if item is not None else None for item in arr]
        else:
            return [
                randint(round(minima / scalar), round(maxima * scalar))
                if item is not None
                else None
                for item in arr
            ]


def replace_floats(arr, scalar=2):
    """Replaces an array of floats with random floats.

    Replaces and array of floats (that can include None's) with random floats.
    None's are kept in place and if the array is ordered, the returned array
    will be ordered as well.

    Args:
        arr: An array to be replaced.
        scalar: An int which represents how much to scale the replaced values by.
            This is dependent on the min and max values of the floats in the input
            array `arr`.
    
    Returns:
        An array with the same lenght as the input array `arr` with randomised
        floats.
    """
    indexes = which_not_none(arr)
    keep = [arr[i] for i in indexes]
    length = len(keep)
    minima, maxima = (min(keep), max(keep))
    if minima == maxima:
        if 0 <= minima <= 1:
            CONST_NUMBERS = [round(uniform(0, 1), 4)] * length
        else:
            CONST_NUMBERS = [
                round(uniform(minima / scalar, maxima * scalar), 4)
            ] * length
        results = replacement_generator(CONST_NUMBERS)
        return [next(results) if n is not None else None for n in arr]
    else:
        if keep == sorted(keep):
            if minima >= 0 and maxima <= 1:
                full_set = [round(uniform(0, 1), 4) for item in keep]
            else:
                full_set = [
                    round(uniform(minima / scalar, maxima * scalar), 4) for item in keep
                ]
            full_set.sort()
            results = replacement_generator(full_set)
            return [next(results) if item is not None else None for item in arr]
        else:
            if minima >= 0 and maxima <= 1:
                return [
                    round(uniform(0, 1), 4) if item is not None else None
                    for item in arr
                ]
            else:
                return [
                    round(uniform(minima / scalar, maxima * scalar), 4)
                    if item is not None
                    else None
                    for item in arr
                ]


def replace_numbers(numbers, scalar=2):
    """Replaces all elements in an array."""
    if not isinstance(numbers, list):
        raise ValueError("passed input is not an array")
    elif (any(isinstance(item, str) for item in numbers)) or (
        all(item is None for item in numbers)
    ):
        return numbers
    else:
        if any(isinstance(item, float) for item in numbers):
            return replace_floats(numbers, scalar)
        elif any(isinstance(item, int) for item in numbers):
            return replace_ints(numbers, scalar)
