<div align="center">
<img src="etc/logo.png" />

![demo](etc/demo.gif)

</div>

A python cli application that obfuscates/randomises numbers (`int`, `float`) in a csv.

**arcanum** expects that each variable is a column in the csv:

<details>
<summary>Example data</summary>

```
date,value_1,value_2,value_3
1970-01-01,2,0.22,
1970-02-01,4,0.99,apple
1970-03-01,5,0.41,
1970-04-01,6,0.1,
1970-05-01,8,0.2,
1970-06-01,11,0.33,orange
1970-07-01,13,0.04,
1970-08-01,15,0.05,
1970-09-01,18,0.01,
1970-10-01,22,0.02,
1970-11-01,,0.09,
1970-12-01,,0.1,
```

</details>

<details>
<summary>Transformed data</summary>

```
date,value_1,value_2,value_3
1970-01-01,9,0.5462,
1970-02-01,11,0.2206,apple
1970-03-01,20,0.4203,
1970-04-01,33,0.66,
1970-05-01,41,0.0367,
1970-06-01,48,0.6726,orange
1970-07-01,53,0.2782,
1970-08-01,55,0.5841,
1970-09-01,59,0.3891,
1970-10-01,78,0.8696,
1970-11-01,,0.0358,
1970-12-01,,0.4366,
```

</details>

and has the following behaviours:

- Columns that are sorted in ascending order will have obfuscated data in ascending order.
- Percentage columns (values between `0.0` and `1.0`) - obfuscated data will be within the same limits.
- All numbers must be `>= 0` (positive) (and converted if not) before they are transformed.
- Strings (any non-empty value that is not an `int` or `float`) is left as is (e.g. such as date columns).
- Arrays that are interspaced with non-numbers will be returned as such (see first column in the example data above).

## Installation and usage

```
pip3 install .
```

### CLI

Once installed, use `arcanum` from the cli:

```
cd ~/jeremy/my-secret-data/
arcanum
```

Because `arcanum` runs recursively, you will want to be make sure that you are in the right folder before invoking the `arcanum` command - you may crash your system if you run from say the home directory of your machine.

Arcanum will also first create a zipped copy of your data in the parent folder of where it is running that will have the folder name and a timestamp suffix. In the case of the above, the file will be located at: `~/jeremy/my-secret-data_20200110_235959.zip`.

### Python module

`arcanum` can also be used as a module in another Python app/script:

```python
import arcanum

my_vals = [1, 2, 3, 4]
my_replaced_vals = arcanum.replace_numbers(my_vals)
```

## Development

Setup a virtual env:

```
python3 -m venv env
source env/bin/activate # OSX.
.\env\Scripts\activate  # Windows.
```

Install in editable mode:

```
pip install -r requirements.txt
pip install -r dev_requirements.txt  # Dev requirements.
```

### Testing

`pytest` is used for testing though coverage is very much lacking at the moment.
