from setuptools import setup

setup(
    name="arcanum",
    version="0.1",
    description="A tool for data obfuscation.",
    packages=["arcanum"],
    url="https://gitlab.com/jeremyyeo/arcanum",
    entry_points={"console_scripts": ["arcanum = arcanum.__main__:main"]},
)
